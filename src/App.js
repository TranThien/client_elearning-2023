import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LayoutDefault from "./HOC/LayoutDefault";
import HomePage from "./Pages/HomePage/HomePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <LayoutDefault>
                <HomePage />
              </LayoutDefault>
            }
          />
          <Route
            path="/detail/course/:id"
            element={
              <LayoutDefault>
                <DetailPage />
              </LayoutDefault>
            }
          />
          <Route
            path="/login"
            element={
              <LayoutDefault>
                <LoginPage />
              </LayoutDefault>
            }
          />
          <Route
            path="/register"
            element={
              <LayoutDefault>
                <RegisterPage />
              </LayoutDefault>
            }
          />
          <Route
            path="*"
            element={
              <LayoutDefault>
                <NotFoundPage />
              </LayoutDefault>
            }
          />
          {/* <Route path="/" /> */}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
