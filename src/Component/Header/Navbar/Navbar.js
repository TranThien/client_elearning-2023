import {
	faBars,
	faCartShopping,
	faMagnifyingGlass,
	faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import styles from "./Navbar.module.scss";

export default function Navbar() {
	const [show, setShow] = useState(false);
	return (
		<div>
			<div className="container px-8 mx-auto py-4 lg:py-2.5">
				<div className="flex justify-between items-center">
					<div className="mr-8">
						<NavLink to="/">
							<img src=".././image/logo.png" alt="" />
						</NavLink>
					</div>
					<div className="hidden lg:flex justify-between items-center grow ">
						<div
							className={`flex justify-center items-center ${styles["container__search"]} `}
						>
							<input
								className={`${styles["form-control"]}`}
								type="text"
								placeholder="Search Course"
							/>
							<FontAwesomeIcon
								className="text-teal-600"
								icon={faMagnifyingGlass}
							/>
						</div>

						<div
							className="hidden w-full md:block md:w-auto"
							id="navbar-default"
						>
							<ul className="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white ">
								<li>
									<a
										href="#"
										className="block py-4  focus:text-teal-600 pl-3 pr-4 text-base hover:scale-x-125 rounded ease-in duration-300 hover:bg-transparent hover:text-teal-700"
										aria-current="page"
									>
										Home
									</a>
								</li>
								<li>
									<a
										href="#"
										className="block py-4 pl-3 pr-4 text-base  focus:text-teal-600 hover:scale-x-125 rounded  ease-in duration-300 hover:bg-transparent hover:text-teal-700
									"
									>
										Course
									</a>
								</li>
								<li>
									<a
										href="#"
										className="block py-4 pl-3 pr-4 text-base  focus:text-teal-600 rounded hover:scale-x-125  hover:bg-transparent ease-in duration-300 hover:text-teal-700"
									>
										Become An Instructor
									</a>
								</li>
							</ul>
						</div>
					</div>
					<ul className="flex justify-between items-center">
						<li className="px-8 md:mr-8 lg:mr-8 lg:border-x-zinc-400  lg:border-y-transparent text-3xl lg:text-2xl lg:border-2">
							<FontAwesomeIcon
								className="text-teal-600"
								icon={faCartShopping}
							/>
						</li>
						<li
							className={`px-10 py-4 text-white rounded ${styles["bg__background"]} transition duration-300 linear hidden lg:block`}
						>
							<button className="block font-semibold text-base">
								Register Now
							</button>
						</li>
						{!show ? (
							<li
								onClick={() => {
									setShow(!show);
								}}
								className="text-3xl px-2.5 py-2 text-teal-600 bg-gray-200 cursor-pointer"
							>
								<FontAwesomeIcon className="block" icon={faBars} />
							</li>
						) : (
							<li
								onClick={() => {
									setShow(!show);
								}}
								className="text-3xl px-2.5 py-2 text-teal-600 bg-gray-200 cursor-pointer"
							>
								<FontAwesomeIcon className="block" icon={faXmark} />
							</li>
						)}
					</ul>
				</div>
			</div>
			{show ? (
				<div className="border-t">
					<ul className="px-6 py-3">
						<li className="mt-3 hover:text-teal-600">
							<NavLink to="/">Home</NavLink>
						</li>
						<li className="mt-3 hover:text-teal-600">Courses</li>
						<li className="mt-3 hover:text-teal-600">Become An Instructor</li>
					</ul>
				</div>
			) : null}
		</div>
	);
}
