import React from "react";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./Top_Header.module.scss";
import { NavLink } from "react-router-dom";

export default function Top_Header() {
	return (
		<div className={`${styles["bg__top-header"]} py-2`}>
			<div
				className={`container ${styles["font-size-15"]} px-3 lg:px-8 mx-auto `}
			>
				<div className=" hidden lg:grid grid-cols-12 leading-8">
					<div className="col-span-8 ">
						<div
							className={`flex items-center ${styles["text__header-color"]}`}
						>
							<p>Keep learning with free resources during COVID-19.</p>
							<a className="tracking-wider font-bold flex items-center" href="">
								Learn more
								<FontAwesomeIcon className="ml-2" icon={faArrowRight} />
							</a>
						</div>
					</div>
					<div className="  col-span-4  text-white  flex items-center justify-end">
						<ul className=" font-semibold tracking-tighter flex">
							<li>Become An Instructor</li>
							<li className={`${styles["li-item"]}`}></li>
							<li className="flex items-center">
								<NavLink to="/login">
									<i className="mr-2">
										<FontAwesomeIcon icon={faArrowRight} />
									</i>
									Signin
								</NavLink>
							</li>
						</ul>
					</div>
				</div>
				<div className="lg:hidden leading-8 ">
					<div className="flex justify-center text-sm">
						<div
							className={` text-sm items-center text-center ${styles["text__header-color"]}`}
						>
							<p className="md:flex">
								Keep learning with free resources during COVID-19.
								<a
									className="tracking-wider font-bold flex items-center"
									href=""
								>
									Learn more
									<FontAwesomeIcon className="ml-2" icon={faArrowRight} />
								</a>
							</p>
						</div>
					</div>
					<div className=" text-white flex items-center justify-center">
						<ul className=" font-semibold tracking-tighter flex">
							<li>Become An Instructor</li>
							<li className={`${styles["li-item"]}`}></li>
							<li className="flex items-center">
								<i className="mr-2">
									<FontAwesomeIcon icon={faArrowRight} />
								</i>
								Signin
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
}
