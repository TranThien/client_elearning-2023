import axios from "axios";
import { userLocalStorage } from "./userLocalStorage";

const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjEwLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NjM1NTIwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg2NTAyODAwfQ.RfqXFAkX0NK9-sSoSak2_Ys49ENugB0G2-zkJO_cEjQ";
export const base_URL = axios.create({
  baseURL: "https://elearningnew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: token,
    // Authorization: "Bearer " + userLocalStorage.get()?.accessToken,
  },
});
