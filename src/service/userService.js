import { base_URL } from "./configURL";

export const userService = {
  postLogin: (data) => {
    return base_URL.post("/api/QuanLyNguoiDung/DangNhap", data);
  },
  postRegister: (data) => {
    return base_URL.post("/api/QuanLyNguoiDung/DangKy", data);
  },
};
